# VERSION defines the project version for the image.
# Update this value when you upgrade the version of your project.
# - use the VERSION as arg of the bundle target (e.g make build VERSION=0.0.2)
# - use environment variables to overwrite this value (e.g export VERSION=0.0.2)
VERSION ?= 0.0.1

# IMAGE_TAG_BASE defines the quay.io namespace and part of the image name for remote images.
# This variable is used to construct full image tags for container images.

IMAGE_TAG_BASE ?= quay.io/pokoradi_1899/static-website-go


# Image URL to use all building/pushing image targets
# IMG ?= controller:latest
# You can use it as an arg. (E.g make build IMG=<some-registry>/<project-name>:<tag>)
IMG ?= $(IMAGE_TAG_BASE):v$(VERSION)

.PHONY: all
all: build push

##@ General

# The help target prints out all targets with their descriptions organized
# beneath their categories. The categories are represented by '##@' and the
# target descriptions by '##'. The awk commands is responsible for reading the
# entire set of makefiles included in this invocation, looking for lines of the
# file as xyz: ## something, and then pretty-format the target and help. Then,
# if there's a line with ##@ something, that gets pretty-printed as a category.
# More info on the usage of ANSI control characters for terminal formatting:
# https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_parameters
# More info on the awk command:
# http://linuxcommand.org/lc3_adv_awk.php

.PHONY: help
help: ## Display this help.
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_0-9-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

##@ Test

.PHONY: run
run: ## Run the server locally.
	go mod tidy && echo -e "Open a separate terminal window and run\n\n 'curl localhost:8080' (to exit press Ctrl+c)\n\n" && go run main.go

##@ Build

.PHONY: build
build: ## Build the container image.
	podman build -t ${IMG} .

.PHONY: push
push: ## Push container image to the registry.
	podman push ${IMG}

.PHONY: container-run
container-run: ## Run the container locally
	echo -e "Open a separate terminal window and run\n\n 'curl localhost:8080' (to exit run  'podman stop ${IMG}')\n\n" && podman run -d --rm -p 8080:8080 ${IMG}

.PHONY: build-run
build-run: ## Build the image and run locally
	$(MAKE) build container-run