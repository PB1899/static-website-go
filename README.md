## Simple static website in Go

A static webserver in Go, built as a container image. For usage, run 
```
make help
```

Should you wish to use it in your project, run
```shell
make build IMG=<your project:your version>
```

I'm using podman as a container runtime, for Docker, update the Makefile targets to

```shell
##@ Build

.PHONY: build
build: ## Build container image with the manager.
	docker build -t ${IMG} -f ./Containerfile

.PHONY: push
push: ## Push container image with the manager.
	docker push ${IMG}
```